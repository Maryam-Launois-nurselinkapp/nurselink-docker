# nurselink-docker

## Table of contents 
* [General informations](#General) 
* [Architecture schema](#schemas) 
* [Getting started](#start)
* [Project deployment](#deployment) 
* [Nurselink application launch](#launch) 




## <a name="General">General informations</a>

The Docker-compose file contains the containerization of :

* Nurselink application APIs ([Nurselink-ms-job](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-job), [Nurselink-ms-user](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-user), [Nurselink-ms-schedule](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-schedule))
* Nurselink web client [Nurselink-web-client](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-web-client)
* the server config [Nurselink-config-server](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-config-server)
* the server gateway [Nurselink-gateway-server](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-gateway-server)
* the Eureka server [Nurselink-eureka-server](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-eureka-server)


## <a name="schemas">Architecture schema</a>


![Nursleink architecture](Nurselink architecture finale.PNG)




## <a name="start">Getting started</a>

Copy one by one all the repositories from https://gitlab.com/Maryam-Launois-nurselinkapp and clone them in a folder called **Nurselinkapp** :

```
mkdir Nurselinkapp
cd Nurselinkapp
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-config-repo.git 
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-config-server.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-docker.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-eureka-server.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-gateway-server.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-job.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-schedule.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-ms-user.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-scripts.git
git clone https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-web-client.git

```

## <a name="deployment">Project deployment</a>


* After launching Docker in your machine
* Type the following command

```
cd Nurselinkapp
cd nurselink-docker
docker-compose build 
docker-compose up
```


## <a name="launch">Nurselink application launch </a>


### 1. Open in a browser the following link which allows you to access to nurse or health establishment portal : http://localhost:8084/      
### 2. You can connect with on of these folowing users :

  ***Users with Nurse role :***

  * Utilisateur 1 			 
    - Identifiant : Assila_Inf 
    - Mot de passe : assila   

  * Utilisateur 2  
    - Identifiant : Cecile_INF 
    - Mot de passe : cecile     

  * Utilisateur 3  
    - Identifiant : Infirmière  
    - Mot de passe : DNVYvSZY79 

  ***Users with HEALTH INSTITUTION role :***

  * Utilisateur 4 
    - Identifiant : CHU_Hautepierre
    - Mot de passe : R7LN9ZRg4K


### 3. You can also access to the admnistrator interface by using the following link : http://localhost:8084/admin/login

  ***Users with ADMINISTRATOR role :***

  * Utilisateur 5
    - Identifiant : Admin
    - Mot de passe : qX5R9BnQ5P


